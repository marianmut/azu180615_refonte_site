#!/usr/bin/env bash

# CLEANUP
if $CONF_setup_cleanup ; then
  printf "${BRN}[=== CLEANUP ===]${NC}\n"
  if $CONF_setup_cleanup_comment ; then
    printf "${BLU}»»» removing default comment...${NC}\n"
    wp comment delete 1 --force
  fi
  if $CONF_setup_cleanup_posts ; then
    printf "${BLU}»»» removing default posts...${NC}\n"
    wp post delete 1 2 --force
  fi
  if $CONF_setup_cleanup_files ; then
    printf "${BLU}»»» removing WP readme/license files...${NC}\n"
    # delete default files
    if [ -f readme.html ];    then rm readme.html;    fi
    if [ -f license.txt ];    then rm license.txt;    fi
    # delete german files
    if [ -f liesmich.html ];  then rm liesmich.html;  fi
  fi
  # Remove les thèmes à la suite de l'install du bon thème, pour éviter de n'avoir aucun thème
  if $CONF_setup_cleanup_themes ; then
    printf "${BLU}»»» removing default themes${NC}\n"
    rm -rf web/wp/wp-content/themes/**
  fi
  if $CONF_setup_cleanup_plugins ; then
    printf "${BLU}»»» removing default plugins${NC}\n"
    rm -rf web/wp/wp-content/plugins/**
  fi
else
  printf "${BLU}>>> skipping Cleanup...${NC}\n"
fi
