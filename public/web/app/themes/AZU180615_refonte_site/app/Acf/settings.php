<?php

return [
  'fields' => [
  ],
  'options' => [
    'main' => [
      'page_title' => 'Theme settings',
      'menu_title' => 'Theme settings',
      'menu_slug' => 'theme-settings',
      'capability' => 'edit_posts',
      'redirect' => false
    ]
  ]
];
