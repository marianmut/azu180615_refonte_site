# Changelog

All update for wpstartiz

## 0.0.4 2019-05-16

### Added
- Monster Insights plugin
- Flamingo plugin

## 0.0.3 2018-12-21

### Changed
- Removed basic ACF

### Added
- Classic Editor plugin

## 0.0.2 2017-11-07

### Changed
- Rename installed theme

### Added
- Changelog file
- Default theme: Sage8 to wpthemetiz