# wpStarTiz - fichier de configuration

Ce guide va permettre d'expliquer le fichier `config.yml` en détaillant ses variables et options.

Le fichier est séparé en 5 sections
* Installation
* Settings
* Theme
* Plugins
* Setup

## Installation

* **`bedrock_folder`** pour définir le dossier où bedrock sera installé
* **`wplocale`** pour définir la langue de WordPress
* **`timezone`** pour définir le fuseau horaire à utiliser
* Les champs **`admin`** permettent de définir l'utilsateur admin par défaut
* **`db`** contient les informations relatives à la base de données. Par défaut (en utilisant la scotchbox) il n'y a pas de changements nécessaires

```
# INSTALLATION
#################################################################

# Bedrock folder
bedrock_folder: public

# language/timezone
wplocale: fr_FR
timezone: "Europe/Paris"

# admin user settings
admin:
  user: tiz
  password: azertiz67
  email: tech@tiz.fr
  first_name: "Agence"
  last_name: "Tiz"

# scotch box db access
db:
  name: scotchbox
  user: root
  pass: root
  prefix: wp_
```

## Settings

* La partie **`wpsettings`** permet de définir les options de WordPress tel que le format d'url, le title, la description, etc
* Définir **`page_on_front`** permet de définir **`frontpage_name`** en tant que page d'accueil
* L'URL doit correspondre à l'url définie dans `VagrantFile` au champ `config.vm.hostname`

```
# SETTINGS
#################################################################

wpsettings:
  url: local.test
  title: Site title
  description: Site description
  permalink_structure: "/%postname%/"
  # use page as frontpage
  page_on_front: true
  # define frontpage name (requires `page_on_front: true`)
  frontpage_name: Accueil
```

## Theme

Cette partie permet de choisir quel thème installer. Il est nécessaire d'indiquer l'url d'un fichier .zip pour que l'installation fonctionne. La variable **`name`** permet de définir le nom qu'aura le thème

```
# THEME
#################################################################

# theme to install
theme:
  name: Sage8
  url: "https://github.com/roots/sage/archive/8.5.4.zip"
```

## Plugins

Il est possible de définir les plugins qui seront installé par wpStarTiz. Les plugins sont séparés en deux sections : les **`plugins_free`**, disponible sur [wpackagist](https://wpackagist.org/), et les **`plugins_premium`** qui ne sont **pas** disponible sur wpackagist. Par défaut, les plugins seront activés après installation.

```
# PLUGINS
#################################################################

# free plugins available on wpackagist
plugins_free:
  - contact-form-7
  - wordpress-seo

# premium plugins not available on wpackagist
plugins_premium:
  - https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip  
```

## Setup

Cette section permet d'effectuer ou non une action du provisioning en définissant la condition à `true` ou `false`.

* **`bedrock`** : installer bedrock et WordPress
* **`settings`** : définir les options de WordPress
* **`themes`** : installer et activer le thème
* **`plugins_free`** : installer les plugins disponibles sur wpackagist
* **`plugins_premium`** : installer les plugins non disponibles sur wpackagist
* **`plugins_activate`** : activer les plugins
* **`cleanup`** : supprimer les éléments par défaut de WordPress
  * **`comment`** : supprimer les commentaires par défaut
  * **`posts`** : supprimer les posts par défaut
  * **`files`** : supprimer `readme.html` et `license.txt`
  * **`themes`** : supprimer les thèmes par défaut
  * **`plugins`** : supprimer les plugins par défaut

```
# WPSTARTIZ SETUP
####################################################################
# if you don't want the setup to run all tasks, set them to false

setup:
  bedrock: true
  settings: true
  theme: true
  plugins_free: true
  plugins_premium: true
  # activate plugins or not
  plugins_activate: true
  cleanup: true
    # adjust what data you want to be deleted within the cleanup (required `cleanup: true`)
    comment: true
    posts: true
    files: true
    themes: true
    plugins: true    
```
