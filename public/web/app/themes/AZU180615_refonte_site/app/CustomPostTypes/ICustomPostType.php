<?php

namespace App\CustomPostTypes;

interface ICustomPostType
{
    public function register();
}
