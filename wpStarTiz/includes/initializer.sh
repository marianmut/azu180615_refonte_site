#!/usr/bin/env bash
#
# REQUIREMENTS
####################################################################################################
#
# YAML PARSER FUNCTION
function parse_yaml() {
    local prefix=$2
    local s
    local w
    local fs
    s='[[:space:]]*'
    w='[a-zA-Z0-9_]*'
    fs="$(echo @|tr @ '\034')"
    sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" "$1" |
    awk -F"$fs" '{
    indent = length($1)/2;
    vname[indent] = $2;
    for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
            vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
            printf("%s%s%s=(\"%s\")\n", "'"$prefix"'",vn, $2, $3);
        }
    }' | sed 's/_=/+=/g'
}

# DEFINE COLORS
RED='\033[0;31m' # error
GRN='\033[0;32m' # success
BLU='\033[0;34m' # task
BRN='\033[0;33m' # headline
NC='\033[0m' # no color

# EXECUTIVE SETUP
####################################################################################################

printf "${BRN}========== WPSTARTIZ START ==========${NC}\n\n"

# MOVE TO SYNCED DIRECTORY
cd ..

# READ CONFIG
eval $(parse_yaml wpStarTiz/config.yml "CONF_")

# CHECK BEDROCK FOLDER
if [ ! -d "$CONF_bedrock_folder" ]; then
  mkdir $CONF_bedrock_folder
  printf "${BLU}»»» creating Bedrock Folder $CONF_bedrock_folder...${NC}\n"
fi

cd $CONF_bedrock_folder
