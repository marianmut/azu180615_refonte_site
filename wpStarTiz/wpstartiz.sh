#!/usr/bin/env bash
#
# WPSTARTIZ setup file
#
# Author: Flurin Dürst
# URL: https://wpdistillery.org
#
# File version 1.7.1

# ERROR Handler
# ask user to continue on error
function continue_error {
  read -p "$(echo -e "${RED}Do you want to continue anyway? (y/n) ${NC}")" -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    printf "\n${RED}»»» aborting WPSTARTIZ setup! ${NC}\n"
    exit 1
  else
    printf "\n${GRN}»»» continuing WPSTARTIZ setup... ${NC}\n"
  fi
}
trap 'continue_error' ERR

. includes/initializer.sh
. ../wpStarTiz/includes/bedrock.sh
. ../wpStarTiz/includes/settings.sh
. ../wpStarTiz/includes/themes_plugins.sh
. ../wpStarTiz/includes/wp_cleanup.sh
