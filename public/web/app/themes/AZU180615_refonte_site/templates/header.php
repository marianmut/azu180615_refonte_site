<header class="banner">
  <div class="grid-container">
    <nav id="main-menu" class="main-menu" aria-label="Main menu">
    <?php wp_nav_menu( array('fallback_cb' => '','menu'=>'primary','container' => true, 'menu_class' => 'nav-primary') ); ?>
  </nav>
  </div>

</header>
