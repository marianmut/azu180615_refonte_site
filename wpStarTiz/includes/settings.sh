#!/usr/bin/env bash

# WP SETTINGS
if $CONF_setup_settings ; then
  printf "${BLU}»»» configure settings...${NC}\n"
  printf "» langue:\n"
  wp language core install $CONF_wplocale
  wp language core activate $CONF_wplocale
  printf "» timezone:\n"
  wp option update timezone $CONF_timezone
  wp option update timezone_string $CONF_timezone
  printf "» date_format:\n"
  wp option update date_format "$CONF_wpsettings_date_format"
  printf "» time_format:\n"
  wp option update time_format "$CONF_wpsettings_time_format"
  printf "» permalink structure:\n"
  wp rewrite structure "$CONF_wpsettings_permalink_structure"
  wp rewrite flush --hard
  printf "» description:\n"
  wp option update blogdescription "$CONF_wpsettings_description"
  if $CONF_wpsettings_page_on_front ; then
    printf "» front page:\n"
    # create and set frontpage
    wp post create --post_type=page --post_title="$CONF_wpsettings_frontpage_name" --post_content='Accueil' --post_status=publish
    wp option update page_on_front $(wp post list --post_type=page --post_status=publish --posts_per_page=1 --pagename="$CONF_wpsettings_frontpage_name" --field=ID --format=ids)
    wp option update show_on_front 'page'
  fi
else
  printf "${BLU}>>> skipping settings...${NC}\n"
fi
