<?php

namespace App\Ajax;

use App\Core\Render;


class Ajax
{
  use Render;

  protected $data;

  private $path = 'templates/ajax/';

  protected function init($public_param, $priv_param = null)
  {
    if ($priv_param === null) {
      $priv_param = $public_param;
    }

    if ($public_param) {
      add_action("wp_ajax_nopriv_$public_param", [$this, $public_param]);
    }
    if ($priv_param) {
      add_action("wp_ajax_$priv_param", [$this, $priv_param]);
    }
  }

}
