#!/usr/bin/env bash
#
# INSTALL BEDROCK
if $CONF_setup_bedrock ; then
  printf "${BRN}[=== INSTALL BEDROCK ===]${NC}\n"
  printf "${BLU}»»» downloading BedRock...${NC}\n"
  composer create-project roots/bedrock .

  printf "${BRN}[=== CONFIGURING DATABASE ACCESS ===]${NC}\n"
  printf "${BLU}»»» Database name => $CONF_db_name ${NC}\n"
  sed -i "s/\("DB_NAME" *= *\).*/\1$CONF_db_name/" .env
  printf "${BLU}»»» Database user => $CONF_db_user ${NC}\n"
  sed -i "s/\("DB_USER" *= *\).*/\1$CONF_db_user/" .env
  printf "${BLU}»»» Database password => ******** ${NC}\n"
  sed -i "s/\("DB_PASSWORD" *= *\).*/\1$CONF_db_pass/" .env
  printf "${BLU}»»» Database prefix => $CONF_db_prefix ${NC}\n"
  sed -i '/DB_PREFIX/s/^#//g' .env
  sed -i "s/\("DB_PREFIX" *= *\).*/\1$CONF_db_prefix/" .env

  printf "${BLU}»»» Site url => $CONF_wpsettings_url ${NC}\n"
  sed -i "s/\("WP_HOME" *= *\).*/\1http:\/\/$CONF_wpsettings_url/" .env

  # delete all leading and trailing whitespaces in .env
  sed -i "s/^[ \t]*//;s/[ \t]*$//" .env

  printf "${BLU}»»» Setting correct DocumentRoot ${NC}\n"
  sudo sed -i 's/DocumentRoot \/var\/www\/public/DocumentRoot \/var\/www\/public\/web/' /etc/apache2/sites-available/000-default.conf
  sudo service apache2 restart

  printf "${BLU}»»» Setting wordpress datas ${NC}\n"
  wp core install --url=$CONF_wpsettings_url --title="$CONF_wpsettings_title" --admin_user=$CONF_admin_user --admin_password=$CONF_admin_password --admin_email=$CONF_admin_email --skip-email
  wp user update 1 --first_name=$CONF_admin_first_name --last_name=$CONF_admin_last_name

  printf "${BLU}»»» Setting Salt Keys ${NC}\n"
  sed -i '/generateme/d' .env
  #Download salts and save to file
  wget -qO /tmp/wp.keys https://api.wordpress.org/secret-key/1.1/salt/
  #parse the key-values into variables
  echo "AUTH_KEY='$(cat /tmp/wp.keys |grep -w AUTH_KEY | cut -d \' -f 4)'" >>  .env
  echo "SECURE_AUTH_KEY='$(cat /tmp/wp.keys |grep -w SECURE_AUTH_KEY | cut -d \' -f 4)'" >>  .env
  echo "LOGGED_IN_KEY='$(cat /tmp/wp.keys |grep -w LOGGED_IN_KEY | cut -d \' -f 4)'" >>  .env
  echo "NONCE_KEY='$(cat /tmp/wp.keys |grep -w NONCE_KEY | cut -d \' -f 4)'" >>  .env
  echo "AUTH_SALT='$(cat /tmp/wp.keys |grep -w AUTH_SALT | cut -d \' -f 4)'" >>  .env
  echo "SECURE_AUTH_SALT='$(cat /tmp/wp.keys |grep -w SECURE_AUTH_SALT | cut -d \' -f 4)'" >>  .env
  echo "LOGGED_IN_SALT='$(cat /tmp/wp.keys |grep -w LOGGED_IN_SALT | cut -d \' -f 4)'" >>  .env
  echo "NONCE_SALT='$(cat /tmp/wp.keys |grep -w NONCE_SALT | cut -d \' -f 4)'" >>  .env
  #remove key file
  rm /tmp/wp.keys
else
  printf "${BLU}>>> skipping Bedrock installation...${NC}\n"
fi
