# wpStarTiz

## Qu'est ce que wpStarTiz ?

wpStarTiz effectue tout le travail lors de l'initilisation d'un nouveau projet WordPress avec la [Scotch Box](https://box.scotch.io/). Il suffit simplement de définir le thème, les plugins, les options, etc. dans le fichier `config.yml`. Puis une simple commande permet de :
* Installer la Scotchbox Pro
* télécharger, installer et configurer [Bedrock](https://roots.io/bedrock/)
* Définire les options de WordPress
* Installer et activer un thème (par défaut [Sage 8](https://github.com/roots/sage/tree/8.5.4))
* Supprimer les thèmes par défaut
* Installer et activer des plugins
* Nettoyer les contenus par défaut de WordPress

Il est possible de définir lesquelles de ces tâches seront éxecutées ou non. Simplement en définissant le paramètre correspondant à `true` ou `false` dans la partie `Setup Options` à la fin du fichier `config.yml`.

## Démarrer un projet

Pour démarrer un nouveau projet, il suffit de suivre ces étapes :

* Cloner le repo depuis [Bitbucket](https://bitbucket.org/tizdev/wpstartiz)
* Configurer les variables dans `wpStarTiz/config` (voir [documentation du fichier de configuration](https://bitbucket.org/tizdev/wpstartiz/src/2459c49b425bd8b9ba32cf160e7f56ef9aaf2aad/README_CONFIG.md?at=master&fileviewer=file-view-default))
* Dans le `VagrantFile`, modifier l'url à `config.vm.hostname` s'il a été modifié dans `config.yml` (il doit être identique au paramètre `wpsettings:url:`)
* Lancer `vagrant up` à la racine du projet

Le projet est maintenant accessible à l'url définie (par défaut : [local.test](http://local.test/)).

Accès au backoffice : site-url/wp/wp-admin (par défaut : [local.test/wp/wp-admin](http://local.test/wp/wp-admin))

## Informations supplémentaires

### Auto-update de WordPress et des plugins

Pour automatiquement mettre à jour WordPress et les plugins à chaque `vagrant up`, il faut dé-commenter la ligne 27 du `VagrantFile`

### Commandes Vagrant

`vagrant up` pour démarrer la VM. Le premier `vagrant up` va installer la scotchbox et éxecuter le provisioning.

`vagrant reload` pour redémarrer la VM. (nécessaire en cas de changements dans le `VagrantFile`).

`vagrant halt` pour arrêter la machine.

`vagrant destroy` pour arrêter et réinitialiser la machine.

## Mise en prod

Pointer sur le dossier `web`.

## À propos

* Version modifiée de [WPDistillery](https://wpdistillery.org/)
* Auteur : [Tiz - Agence de communication](https://www.tiz.fr/)

## Évolutions futures

* Gestion de l'installation des plugins premiums via Composer

## Suivi du projet

* [Trello](https://trello.com/b/oPZdOyEV/wp-squelette)
* [Cahier des charges](https://docs.google.com/document/d/1heuZ6hfkGslsgeC352ZAhVAogHX1lmLZJ0dMsjBpWwY/edit?usp=sharing)
