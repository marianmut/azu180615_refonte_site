<?php
use App\Acf\RegisterFields;
use App\Acf\Search;
use App\Override\Gallery;
use App\Override\WysiwygStyles;
use App\Ajax\Exemple as ExempleAjax;

(new RegisterFields())->load();
(new Search())->load();
(new Gallery())->hooks();
(new WysiwygStyles())->hooks();

$cpts = new App\CustomPostTypes\Manager();
  // $cpts->add(new App\CustomPostTypes\Exemples());
$cpts->registers();

// $exempleAjax = new ExempleAjax();
// $exempleAjax->load();
