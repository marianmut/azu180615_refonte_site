<?php

namespace App\Acf;

class RegisterFields
{
    private $settings;

    public function __construct()
    {
        $this->settings = include(__DIR__ . '/settings.php');
    }

    public function load()
    {
        if (function_exists('acf_add_local_field_group')) :
            foreach ($this->settings['fields'] as $setting) {
            acf_add_local_field_group($setting);
        }

        if (isset($this->settings['options']) && !empty($this->settings['options'])) {
            foreach ($this->settings['options'] as $option) {
                acf_add_options_page($option);
            }
        }

        endif;
    }
}
