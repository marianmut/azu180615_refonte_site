<div class="gallery">
  <div class="grid-x grid-padding-x grid-margin-y">
    <?php foreach($attachments as $id => $attachment): ?>
    <div class="cell small-12 medium-6 large-<?= ceil(12 / $atts['columns']) ?>">
      <figure class="gallery__item">
        <?php if(!empty($atts['link']) && 'file' === $atts['link']): ?>
          <?= wp_get_attachment_link($id, $atts['size'], false, false, false); ?>
        <?php elseif(!empty($atts['link']) && 'none' === $atts['link']): ?>
          <?= wp_get_attachment_image($id, $atts['size'], false); ?>
        <?php else: ?>
          <?= wp_get_attachment_link($id, $atts['size'], true, false, false); ?>
        <?php endif; ?>

        <?php if (trim($attachment->post_excerpt)): ?>
        <figcaption class="gallery__caption">
          <?= wptexturize($attachment->post_excerpt) ?>
        </figcaption>
        <?php endif; ?>
      </figure>
    </div>
    <?php endforeach; ?>
  </div>
</div>
