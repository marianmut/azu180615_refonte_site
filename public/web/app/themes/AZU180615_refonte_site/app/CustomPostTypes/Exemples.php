<?php

namespace App\CustomPostTypes;

class Exemples extends CustomPostType implements ICustomPostType
{


  public function register()
  {
    $this->init();
    $this->registerTaxoCategory();
  }

  private function init()
  {
    $this->registerPostType([
      'post_type_name' => 'exemple',
      'singular' => 'Exemple',
      'plural' => 'Exemples',
      'slug' => false,
    ], [
      'public' => false,
      'show_in_nav_menus' => true,
      'publicly_queryable' => false,
      'show_ui' => true,
      'exclude_from_search' => false,
      'has_archive' => false,
      'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
    ]);
  }

  private function registerTaxoCategory()
  {
    $this->registerTaxonomy([
      'taxonomy_name' => 'exemple_category',
      'singular' => 'Exemples Category',
      'plural' => 'Exemples Categories',
      'slug' => false,
    ]);
  }
}
