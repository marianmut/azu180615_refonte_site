<?php

namespace App\Core;

trait Render {
  function render($tpl, $params = [])
  {
    ob_start();
    extract($params);
    include(locate_template($this->path . $tpl . '.php'));
    return ob_get_clean();
  }
}
