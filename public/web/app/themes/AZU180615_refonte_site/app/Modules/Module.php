<?php

namespace App\Modules;

use App\Core\Render;

class Module
{
  use Render;

  protected static $settings;
  private $path = 'templates/modules/';

  private static function getSettings()
  {
    if (!self::$settings)
      self::$settings = include(__DIR__ . '/settings.php');

    return self::$settings;
  }
}
