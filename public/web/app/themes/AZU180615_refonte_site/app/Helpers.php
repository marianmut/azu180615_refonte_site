<?php

namespace App;

use App\Core\Render;

class Helpers
{
  use Render;

  private $path = 'templates/helpers/';

  public static $instance;

  public static function linkACF($link, $attr = [])
  {
    if($link) {
      if ($link['target'])
        $attr['target'] = $link['target'];

      if ($link['target'] == '_blank')
        $attr['rel'] = 'noopener noreferrer';

      $attr = self::attrHtml($attr);

      return ($link) ? self::rendered('link', compact('link', 'attr')) : '';
    }
  }

  private static function attrHtml($_attr)
  {

    $_attr = array_map('esc_attr', $_attr);
    $attr = '';
    foreach ($_attr as $name => $value) {
      $attr .= " $name=" . '"' . $value . '"';
    }
    return $attr;
  }

  private static function rendered($tpl, $params = [])
  {
    if (!isset(self::$instance) || empty(self::$instance)) {
      self::$instance = new Helpers;
    }
    return self::$instance->render($tpl, $params);
  }

  public static function gravityForm($id, $title = 'false', $description = 'false', $ajax = 'true')
  {
    return do_shortcode('[gravityform id="' . $id . '" title="' . $title . '" description="' . $description . '" ajax="' . $ajax . '"]');
  }

  public static function gravityFormLock($id, $title = 'false', $description = 'false', $ajax = 'true')
  {
    return do_shortcode('[dlm_gf_form download_id="' . $id . '" gf_title="' . $title . '" gf_description="' . $description . '" gf_ajax="' . $ajax . '"]');
  }

  public static function embed($iframe, $params = [])
  {
    // use preg_match to find iframe src
    preg_match('/src="(.+?)"/', $iframe, $matches);
    $src = $matches[1];

    // add extra params to iframe src
    $new_src = add_query_arg($params, $src);
    $iframe = str_replace($src, $new_src, $iframe);

    // add extra attributes to iframe html
    $attributes = 'frameborder="0"';
    $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

    return $iframe;
  }

  public static function embedYT($iframe, $params = [])
  {
    // add extra params to iframe src
    $_params = array(
      'controls' => 1,
      'hd' => 1,
      'color' => 'white',
      'modestbranding' => 1,
      'rel' => 0,
      'showinfo' => 0
    );
    $params = array_merge($_params, $params);

    return self::embed($iframe, $params);
  }
}
