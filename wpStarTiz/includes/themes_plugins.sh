#!/usr/bin/env bash

# INSTALL THEME
if $CONF_setup_theme ; then
  printf "${BRN}[=== INSTALL $CONF_theme_name ===]${NC}\n"
  printf "${BLU}»»» downloading, installing and activating $CONF_theme_name...${NC}\n"
  wp theme install $CONF_theme_url --activate

  theme_name=$(wp theme list --status=active --field=name)

  if [ ! -z "$CONF_theme_name" ];  then
    # rename theme
    printf "${BLU}»»» renaming $theme_name to $CONF_theme_name...${NC}\n"
    mv web/app/themes/$theme_name web/app/themes/$CONF_theme_name
    wp theme activate $CONF_theme_name
    theme_name=$CONF_theme_name
  fi

  cd web/app/themes/$theme_name
  composer install 
  cd ../../../../
else
  printf "${BLU}>>> skipping theme installation...${NC}\n"
fi

# PLUGINS
printf "${BRN}[=== PLUGINS ===]${NC}\n"
if $CONF_setup_plugins_free ; then
  printf "${BLU}»»» adding free plugins${NC}\n"
  for entry in "${CONF_plugins_free[@]}"
  do
    composer require wpackagist-plugin/$entry
  done
else
  printf "${BLU}»»» skipping free Plugin installation...${NC}\n"
fi

if $CONF_setup_plugins_premium ; then
  printf "${BRN}[=== PLUGINS ===]${NC}\n"
  printf "${BLU}»»» adding premium plugins${NC}\n"
  for entry in "${CONF_plugins_premium[@]}"
  do
    wp plugin install $entry
  done
else
  printf "${BLU}»»» skipping premium Plugin installation...${NC}\n"
fi

if "$CONF_setup_plugins_activate" ; then
  printf "${BLU}»»» activating plugins${NC}\n"
  wp plugin activate --all
else
  printf "${BLU}»»» skipping plugins activation${NC}\n"
fi
