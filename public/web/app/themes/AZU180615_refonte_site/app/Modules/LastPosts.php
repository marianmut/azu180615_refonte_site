<?php

namespace App\Modules;

class LastPosts extends Module
{

  public function display($count, $categories = [], $tpl = 'last-posts')
  {
    $render = '';
    $lastPosts = $this->getLastPosts($count, $categories);
    if ($lastPosts) {
      $render = $this->render($tpl, compact('lastPosts'));
    }
    wp_reset_postdata();

    return $render;
  }

  private function getLastPosts($count, $categories = [])
  {
    global $post;

    $category = implode(',', array_map(function($cat){
      return $cat->term_id;
    }, $categories));

    $recent_posts = wp_get_recent_posts([
      'numberposts' => $count,
      'exclude' => $post->ID,
      'category' => $category
    ], OBJECT);

    return $recent_posts;
  }
}
