<?php

namespace App\CustomPostTypes;

class CustomPostType
{
  protected $cpt;

  protected function registerPostType($postTypeNames, $options = [])
  {
    $this->cpt = new \CPT($postTypeNames, $options);
  }

  protected function registerTaxonomy($taxonomy)
  {
    $this->cpt->register_taxonomy($taxonomy);
  }

}
